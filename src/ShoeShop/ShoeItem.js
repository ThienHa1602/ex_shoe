import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left">
          <img
            className="card-img-top"
            style={{ width: "10vw" }}
            src={this.props.dataShoe.image}
          />
          <div className="card-body text-center">
            <h4 className="card-title">{this.props.dataShoe.price}</h4>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.dataShoe);
              }}
              className="btn btn-success"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.hanldeAddToCart(this.props.dataShoe);
              }}
              className="btn btn-warning"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
