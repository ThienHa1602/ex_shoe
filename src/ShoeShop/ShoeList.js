import React, { Component } from "react";
import ShoeItem from "./ShoeItem";

export default class ShoeList extends Component {
  renderShoeList = () => {
    return this.props.list.map((item, index) => {
      return (
        <ShoeItem
          dataShoe={item}
          key={index}
          handleChangeDetail={this.props.handleChangeDetail}
          hanldeAddToCart={this.props.hanldeAddToCart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderShoeList()}</div>;
  }
}
