import React, { Component } from "react";
import { shoeArr } from "./dataShoe";
import ShoeCart from "./ShoeCart";
import ShoeDetail from "./ShoeDetail";
import ShoeList from "./ShoeList";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    shoeDetail: shoeArr[0],
    cart: [],
  };
  handleChangeDetail = (shoe) => {
    this.setState({ shoeDetail: shoe });
  };
  hanldeAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    var index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <ShoeCart
              cart={this.state.cart}
              handleDelete={this.handleDelete}
              handleChangeQuantity={this.handleChangeQuantity}
            />
          </div>
          <div className="col-4">
            <ShoeList
              list={this.state.shoeArr}
              handleChangeDetail={this.handleChangeDetail}
              hanldeAddToCart={this.hanldeAddToCart}
            />
          </div>
        </div>
        <ShoeDetail shoeDetail={this.state.shoeDetail} />
      </div>
    );
  }
}
